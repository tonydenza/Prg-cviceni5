﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni5._2
{
    class Program
    {
        static void Switch (ref int first, ref int second)
        {
            int tmp = first;
            first = second;
            second = tmp;
        }

        static void QuicksortRec (ref int[] pole, int left, int right)
        {
            int pivot = pole[left], left_s = left, right_s = right;

            if (left < right)
            {
                while (left_s < right_s)
                {
                    if (pole[left_s] < pivot) left_s++;
                    else if (pole[right_s] > pivot) right_s--;
                    else Switch(ref pole[left_s], ref pole[right_s]);
                }
                QuicksortRec(ref pole, left, left_s - 1);
                QuicksortRec(ref pole, left_s + 1, right);
            }

        }

        static void Fuse(ref int[] pole, int left, int right, int half)
        {
            int[] pom = new int[pole.Length];
            int first = left, first_s = half + 1, index = left, count = (right - left + 1);

            while (first <= half && first_s <= right)
            {
                if (pole[first] <= pole[first_s])
                    pom[index++] = pole[first++];
                else
                    pom[index++] = pole[first_s++];
            }

            while (first <= half) pom[index++] = pole[first++];
            while (first_s <= right) pom[index++] = pole[first_s++];
            for (int i = 0; i < count; i++) pole[right] = pom[right--];
        }

        static void MergesortRec (ref int[] pole, int left, int right)
        {
            if (left < right)
            {
                int half = (left + right) / 2;
                MergesortRec(ref pole, left, half);
                MergesortRec(ref pole, half+1, right);
                Fuse(ref pole, left, right, half);
            }
        }

        static void PrintArray (int[] array)
        {
            for (int i = 0; i < array.Length; i++)
                Console.Write("{0} ", array[i]);
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            int[] pole  = { 15, 12, 9, 17, 4, 7, 11, 32, 1, 8, 5, 19, 20, 2, 6, 18, 13, 37, 3, 10 };
            int[] pole2 = { 15, 12, 9, 17, 4, 7, 11, 32, 1, 8, 5, 19, 20, 2, 6, 18, 13, 37, 3, 10 };
            QuicksortRec(ref pole,  0, pole.Length - 1);
            MergesortRec(ref pole2, 0, pole.Length - 1);
            PrintArray(pole);
            PrintArray(pole2);
        }
    }
}
